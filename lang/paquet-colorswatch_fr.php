<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'colorswatch_description' => 'Définir des palettes de couleur',
	'colorswatch_nom' => 'Color swatch',
	'colorswatch_slogan' => 'Définir une palette de couleurs',
);
