<?php
/**
 * Options au chargement du plugin Color swatch
 *
 * @plugin     Color swatch
 * @copyright  2019
 * @author     Mist. GraphX
 * @licence    GNU/GPL
 * @package    SPIP\Colorswatch\Options
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
