<?php
/**
 * Utilisations de pipelines par Color swatch
 *
 * @plugin     Color swatch
 * @copyright  2019
 * @author     Mist. GraphX
 * @licence    GNU/GPL
 * @package    SPIP\Colorswatch\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * pipeline social_networks
 *
 *
*/
function colorswatch_user_colors($flux){

	return $flux;
}

/**
 * Insertion css,js privé
 *
 * @param $flux
 * @return string
 *
 */
function colorswatch_header_prive($flux){
	$flux .= '<link rel="stylesheet" type="text/css" href="'.find_in_path('css/colorswatch.css').'" />' . "\n";;
	$flux .= '<script src="'.produire_fond_statique('js/colorswatch.js').'" type="text/javascript"></script>'."\n";
	return $flux;
}
