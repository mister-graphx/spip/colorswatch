# Colorswatch

Définir une palette

| Clé | Valeur |
| :-- | :-- |
| id | selecteur ou id unique |
|name | nom lisible |
|value | valeur hexa (`#FFFFFF`)|

## Configuration

jquery_selectors : input:color sur lesquels on remplace le colorpiker system.


## Récupérer le colorswatch en js

```javascript
jQuery(function($){

  $.getJSON("#URL_PAGE{colorswatch.json}").done(function(data) {
    console.log(data);
  }).fail(function() {
    console.log( "error" );
  });

});
```

## Pipeline `user_colors`

Déclarer un color swatch depuis le fichier `_options.php`


```php
$GLOBALS['spip_pipeline']['user_colors'] .= "|prefix_colorswatch";
function prefix_colorswatch($colors) {
	$colors = array(
		'primary'=> array(
			'name'=> 'Couleur 1',
			'value'=> "#1F377F"
		),
		'secondary'=> array(
			'name'=> 'Couleur 2',
			'value'=> "#FF7851"
		),
		'tertiary'=> array(
			'name'=> 'Couleur 3',
			'value'=> "#ED368D"
		),
		'quadrary'=> array(
			'name'=> 'Couleur 4',
			'value'=> "#0D9CFF"
		),
		'complementary'=>array(
			'name'=>'Complementaire',
			'value'=>'#FFCA47'
		)
	);
  return $colors;
}
```
